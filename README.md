
**NEWS**

  * ** v0.4 ALPHA **
    * [IMPROVEMENT] Crypthography, one button for both encryption and decryption
    * [FEATURE] Cancel button
    * [FIX] Fail when using encrypt/decrypt operation on nonexistent field
    
  * ** v0.31 ALPHA **
     * [FIX] Fail when using encrypt operation on nonexistent field
     
 * ** v0.3 ALPHA **
     * [FEATURE] Cryptography

  * ** v0.21 ALPHA **
     * [FIX] If settings file not exists settings will be restored to default
     
  * ** v0.2 ALPHA **
    * [FEATURE] Settings dialog
    * [FEATURE] Font changing
    
