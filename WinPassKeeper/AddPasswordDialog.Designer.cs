﻿namespace WinPassKeeper
{
    partial class AddPasswordDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddPasswordDialog));
            this.btnAddPassword = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.editPassword = new System.Windows.Forms.TextBox();
            this.editDescription = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelUsername = new System.Windows.Forms.Label();
            this.editUsername = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAddPassword
            // 
            resources.ApplyResources(this.btnAddPassword, "btnAddPassword");
            this.btnAddPassword.Name = "btnAddPassword";
            this.btnAddPassword.UseVisualStyleBackColor = true;
            this.btnAddPassword.Click += new System.EventHandler(this.btnAddPassword_Click);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // editPassword
            // 
            resources.ApplyResources(this.editPassword, "editPassword");
            this.editPassword.Name = "editPassword";
            // 
            // editDescription
            // 
            resources.ApplyResources(this.editDescription, "editDescription");
            this.editDescription.Name = "editDescription";
            // 
            // labelPassword
            // 
            resources.ApplyResources(this.labelPassword, "labelPassword");
            this.labelPassword.Name = "labelPassword";
            // 
            // labelDescription
            // 
            resources.ApplyResources(this.labelDescription, "labelDescription");
            this.labelDescription.Name = "labelDescription";
            // 
            // labelUsername
            // 
            resources.ApplyResources(this.labelUsername, "labelUsername");
            this.labelUsername.Name = "labelUsername";
            // 
            // editUsername
            // 
            resources.ApplyResources(this.editUsername, "editUsername");
            this.editUsername.Name = "editUsername";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // AddPasswordDialog
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.editUsername);
            this.Controls.Add(this.labelUsername);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.editDescription);
            this.Controls.Add(this.editPassword);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAddPassword);
            this.Name = "AddPasswordDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddPassword;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox editPassword;
        private System.Windows.Forms.TextBox editDescription;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.TextBox editUsername;
        private System.Windows.Forms.Label label1;

    }
}