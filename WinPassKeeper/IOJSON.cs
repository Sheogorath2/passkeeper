﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WinPassKeeper
{
    class IOJSON
    {
        public static Newtonsoft.Json.JsonSerializerSettings InitJSS()  {
            Newtonsoft.Json.JsonSerializerSettings jss;
            jss = new Newtonsoft.Json.JsonSerializerSettings();

            jss.Formatting = Newtonsoft.Json.Formatting.Indented;

            Newtonsoft.Json.Serialization.DefaultContractResolver dcr = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            dcr.DefaultMembersSearchFlags |= System.Reflection.BindingFlags.NonPublic;
            jss.ContractResolver = dcr;

            return jss;
        }

        public static String JSONizeModel(IModel model) {
            Newtonsoft.Json.JsonSerializerSettings jss = InitJSS();
            Model fullModel = new Model(model);
            return Newtonsoft.Json.JsonConvert.SerializeObject(fullModel, jss);
        }

        public static IModel deJSONizeModel(String json) {
            Newtonsoft.Json.JsonSerializerSettings jss = InitJSS();

            Model fullModel = JsonConvert.DeserializeObject<Model>(json, jss);
            return fullModel;
        }

        public static String JSONizeSettings(Settings settings) {
            Newtonsoft.Json.JsonSerializerSettings jss = InitJSS();
            return Newtonsoft.Json.JsonConvert.SerializeObject(settings, jss);
        }

        public static Settings deJSONizeSettings(String json) {
            Newtonsoft.Json.JsonSerializerSettings jss = InitJSS();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Settings>(json, jss);
        }
    }
}
