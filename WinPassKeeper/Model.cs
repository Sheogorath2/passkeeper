﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinPassKeeper
{
    class Model : IModel
    {
        private IList<Password> passwords = new List<Password>();

        public Model() {

        }

        public Model(IModel model) {
            for (int i = 0; i < model.Count(); i++ )
                addPassword(model.getPassword(i));
        }

        public Password getPassword(int index)  {
            return passwords[index];
        }

        public void addPassword(Password password) {
            passwords.Add(password);
        }

        public void removePassword(int index)      {
            passwords.RemoveAt(index);
        }

        public override string ToString() {
            String str = "Model with passwords:\n";
            foreach (Password pass in passwords)
                str += pass.password + "\t" + pass.description+ "\n";
            return str;
        }

        public int Count() {
            return passwords.Count;
        }
    }
}
