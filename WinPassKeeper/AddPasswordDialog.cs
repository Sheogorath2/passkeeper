﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinPassKeeper
{
    public partial class AddPasswordDialog : Form
    {
        private IController controller;
        private Boolean addNew;
        private int selected;

        public void registerObserver(IController controller) {
            this.controller = controller;
        }

        public AddPasswordDialog(Boolean addNew = true, int selected = 0,
            String description = "", String username = "", String password = "") {

            InitializeComponent();

            this.addNew = addNew;
            this.selected = selected;
            this.editDescription.Text = description;
            this.editUsername.Text = username;
            this.editPassword.Text = password;

            if (addNew) 
                Text = "Add new password";
            else {
                Text = "Edit existing password";
                btnAddPassword.Text = "Apply changes";
            }
        }

        private void btnAddPassword_Click(object sender, EventArgs e) {
            if (addNew)
                controller.ProcessAddEntry(editDescription.Text, editUsername.Text, editPassword.Text);
            else
                controller.ProcessChangeEntry(selected, editDescription.Text, editUsername.Text, editPassword.Text);
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            Close();
        }

    }
}
