﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinPassKeeper {
    public partial class SettingsDialog : Form {

        private IController controller;
        private Settings settings = new Settings(); 

        public void registerObserver(IController controller) {
            this.controller = controller;
        }

        public SettingsDialog() {
            InitializeComponent();
            //settings = controller.getSettings();
            try {
                settings = IOJSON.deJSONizeSettings(System.IO.File.ReadAllText("settings.json"));
            }
            catch (Exception) {
                settings.restoreDefault();
            }
            
            trackFontSize.Value = settings.fontSize;
        }

        private void trackFontSize_Scroll(object sender, EventArgs e) {
            settings.fontSize = trackFontSize.Value;
        }

        private void btnApply_Click(object sender, EventArgs e) {
            controller.setSettings(settings);
            controller.appllySettings();
            controller.saveSettings();
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            Close();
        }
    }
}
