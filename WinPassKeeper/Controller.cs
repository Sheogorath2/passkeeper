﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;

namespace WinPassKeeper {
    class Controller : IController {
        private IModel model;
        private IView view;
        private Settings settings = new Settings();
        Model uncrypted;
        bool wasCryptingOperation{get;set;}

        public Controller(IModel model, IView view) {
            this.model = model;
            this.view = view;
            loadSettings();
            appllySettings();
        }

        public void modelToFile(String filename) {
            //System.IO.StreamWriter file = new System.IO.StreamWriter(filename);
            System.IO.File.WriteAllText(filename, IOJSON.JSONizeModel(model));
        }

        public void modelFromFile(String filename) {
            model = IOJSON.deJSONizeModel(System.IO.File.ReadAllText(filename));
        }

        //Test method
        public void run() {
            //Password p1 = new Password("example password 1", "p1");
            //Password p2 = new Password("example password 2", "p2");
            //model.addPassword(p1);
            //model.addPassword(p2);
            //model.addPassword(new Password("This is the password", "the password"));
            //System.IO.Directory.CreateDirectory("passwords");
            //modelToFile("passwords/test.json");

            //modelFromFile("passwords/test.json");
            //Console.WriteLine(model);
            //view.updateAll();
            ////modelFromFile("passwords/custom.json");
            ////Console.WriteLine(model);
        }

        public String[] getPasswords() {
            String[] passArray = new String[model.Count()];
            for (int i = 0; i < model.Count(); i++)
                passArray[i] = this.model.getPassword(i).description + "  :  " + model.getPassword(i).username + "  :  " + model.getPassword(i).password;
            return passArray;
        }

        public Password getPassword(int index) {
            return model.getPassword(index);
        }

        public void loadSettings() {
            try {
                settings = IOJSON.deJSONizeSettings(System.IO.File.ReadAllText("settings.json"));
            }
            catch (Exception) {

                settings.restoreDefault();
            }
        }

        public void saveSettings() {
            System.IO.File.WriteAllText("settings.json", IOJSON.JSONizeSettings(settings));
        }

        public void appllySettings() {
            view.changeFontSize(settings.fontSize);
        }

        public Settings getSettings() {
            return settings;
        }

        public void setSettings(Settings settings) {
            this.settings = settings;
        }

        public bool getCryptionStatus() {return wasCryptingOperation;}

        // Processes
        public void ProcessLoadFile() {
            String filename = view.getOpenJSONFileName();
            if (filename != "") {
                modelFromFile(filename);
                view.updateAll();
            }
        }

        public void ProcessSaveFile() {
            String filename = view.getSaveJSONFileName();
            if (filename != "") {
                modelToFile(filename);
                view.updateAll();
            }
        }

        public void ProcessClearFile() {
            if (view.checkFileClear())
                model = new Model();
            view.updateAll();
        }

        public void ProcessAddEntry(String description, String username, String password) {
            if (password != "" || description != "") {
                model.addPassword(new Password(description, username, password));
                view.updateAll();
                view.selectElement(model.Count() - 1);
            }
        }

        public void ProcessDelEntry(int index) {
            if (index >= 0) {
                if (view.checkPasswordDeletion()) {
                    model.removePassword(index);
                    view.updateAll();
                    if (index > 0)
                        view.selectElement(index - 1);
                }
            }
        }

        public void ProcessChangeEntry(int index, String description, String username, String password) {
            Password pass = new Password(description, username, password);
            IModel tempModel = new Model();
            for (int i = 0; i < model.Count(); i++) {
                if (i != index)
                    tempModel.addPassword(model.getPassword(i));
                else
                    tempModel.addPassword(pass);
            }
            model = tempModel;
            view.updateAll();
            view.selectElement(index);
        }

        public void ProcessCryptEntry(int index, String CrPassword) {
            uncrypted = new Model();
            for (int i = 0; i < model.Count(); i++)
                uncrypted.addPassword(model.getPassword(i));
            wasCryptingOperation = true;
            String description = StringCypher.EncryptOrDecrypt(model.getPassword(index).description, CrPassword);
            String username = StringCypher.EncryptOrDecrypt(model.getPassword(index).username, CrPassword);
            String password = StringCypher.EncryptOrDecrypt(model.getPassword(index).password, CrPassword);
            ProcessChangeEntry(index, description, username, password);
        }

        public void ProcessCryptEntries(String CrPassword) {
            uncrypted = new Model();
            for (int i = 0; i < model.Count(); i++)
                uncrypted.addPassword(model.getPassword(i));
            wasCryptingOperation = true;
            for (int i = 0; i < model.Count(); i++) {
                ProcessCryptEntry(i, CrPassword);
            }
        }

        public void ProcessCancelCryption() {
            model = new Model();
            for (int i = 0; i < uncrypted.Count(); i++)
                model.addPassword(uncrypted.getPassword(i));
            wasCryptingOperation = false;
            view.updateAll();
        }

    }
}