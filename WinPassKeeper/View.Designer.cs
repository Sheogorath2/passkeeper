﻿namespace WinPassKeeper
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemNew = new System.Windows.Forms.ToolStripMenuItem();
            this.itemLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.entryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.itemRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.itemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.securityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemCryptography = new System.Windows.Forms.ToolStripMenuItem();
            this.itemCrAll = new System.Windows.Forms.ToolStripMenuItem();
            this.itemCrCurrent = new System.Windows.Forms.ToolStripMenuItem();
            this.itemCancel = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(0, 57);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(670, 316);
            this.listBox1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.entryToolStripMenuItem,
            this.securityToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(670, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemNew,
            this.itemLoad,
            this.itemSave,
            this.itemSettings});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // itemNew
            // 
            this.itemNew.Image = global::WinPassKeeper.Properties.Resources.New_document;
            this.itemNew.Name = "itemNew";
            this.itemNew.Size = new System.Drawing.Size(116, 22);
            this.itemNew.Text = "&New";
            this.itemNew.Click += new System.EventHandler(this.itemNew_Click);
            // 
            // itemLoad
            // 
            this.itemLoad.Image = global::WinPassKeeper.Properties.Resources.Folder;
            this.itemLoad.Name = "itemLoad";
            this.itemLoad.Size = new System.Drawing.Size(116, 22);
            this.itemLoad.Text = "&Load";
            this.itemLoad.Click += new System.EventHandler(this.itemLoad_Click);
            // 
            // itemSave
            // 
            this.itemSave.Image = global::WinPassKeeper.Properties.Resources.Save;
            this.itemSave.Name = "itemSave";
            this.itemSave.Size = new System.Drawing.Size(116, 22);
            this.itemSave.Text = "&Save";
            this.itemSave.Click += new System.EventHandler(this.itemSave_Click);
            // 
            // itemSettings
            // 
            this.itemSettings.Image = global::WinPassKeeper.Properties.Resources.Pinion;
            this.itemSettings.Name = "itemSettings";
            this.itemSettings.Size = new System.Drawing.Size(116, 22);
            this.itemSettings.Text = "&Settings";
            this.itemSettings.Click += new System.EventHandler(this.itemSettings_Click);
            // 
            // entryToolStripMenuItem
            // 
            this.entryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemAdd,
            this.itemRemove,
            this.itemEdit});
            this.entryToolStripMenuItem.Name = "entryToolStripMenuItem";
            this.entryToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.entryToolStripMenuItem.Text = "Entry";
            // 
            // itemAdd
            // 
            this.itemAdd.Image = global::WinPassKeeper.Properties.Resources.Add;
            this.itemAdd.Name = "itemAdd";
            this.itemAdd.Size = new System.Drawing.Size(117, 22);
            this.itemAdd.Text = "&Add";
            this.itemAdd.Click += new System.EventHandler(this.itemAdd_Click);
            // 
            // itemRemove
            // 
            this.itemRemove.Image = global::WinPassKeeper.Properties.Resources.Delete;
            this.itemRemove.Name = "itemRemove";
            this.itemRemove.Size = new System.Drawing.Size(117, 22);
            this.itemRemove.Text = "&Remove";
            this.itemRemove.Click += new System.EventHandler(this.itemRemove_Click);
            // 
            // itemEdit
            // 
            this.itemEdit.Image = global::WinPassKeeper.Properties.Resources.Modify;
            this.itemEdit.Name = "itemEdit";
            this.itemEdit.Size = new System.Drawing.Size(117, 22);
            this.itemEdit.Text = "&Edit";
            this.itemEdit.Click += new System.EventHandler(this.itemEdit_Click);
            // 
            // securityToolStripMenuItem
            // 
            this.securityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemCryptography,
            this.itemCancel});
            this.securityToolStripMenuItem.Name = "securityToolStripMenuItem";
            this.securityToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.securityToolStripMenuItem.Text = "Security";
            // 
            // itemCryptography
            // 
            this.itemCryptography.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemCrAll,
            this.itemCrCurrent});
            this.itemCryptography.Image = global::WinPassKeeper.Properties.Resources.Lock;
            this.itemCryptography.Name = "itemCryptography";
            this.itemCryptography.Size = new System.Drawing.Size(171, 22);
            this.itemCryptography.Text = "Encrypt or decrypt";
            // 
            // itemCrAll
            // 
            this.itemCrAll.Image = global::WinPassKeeper.Properties.Resources.all;
            this.itemCrAll.Name = "itemCrAll";
            this.itemCrAll.Size = new System.Drawing.Size(114, 22);
            this.itemCrAll.Text = "All";
            this.itemCrAll.Click += new System.EventHandler(this.itemCrAll_Click);
            // 
            // itemCrCurrent
            // 
            this.itemCrCurrent.Image = global::WinPassKeeper.Properties.Resources.entry1;
            this.itemCrCurrent.Name = "itemCrCurrent";
            this.itemCrCurrent.Size = new System.Drawing.Size(114, 22);
            this.itemCrCurrent.Text = "Current";
            this.itemCrCurrent.Click += new System.EventHandler(this.itemCrCurrent_Click);
            // 
            // itemCancel
            // 
            this.itemCancel.Enabled = false;
            this.itemCancel.Image = global::WinPassKeeper.Properties.Resources.Cancel;
            this.itemCancel.Name = "itemCancel";
            this.itemCancel.Size = new System.Drawing.Size(171, 22);
            this.itemCancel.Text = "&Cancel";
            this.itemCancel.Click += new System.EventHandler(this.itemCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(0, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 8;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(0, 0);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 7;
            // 
            // btnEdit
            // 
            this.btnEdit.Image = global::WinPassKeeper.Properties.Resources.Modify;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEdit.Location = new System.Drawing.Point(93, 28);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Image = global::WinPassKeeper.Properties.Resources.Delete;
            this.btnRemove.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnRemove.Location = new System.Drawing.Point(583, 27);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 5;
            this.btnRemove.Text = "   &Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = global::WinPassKeeper.Properties.Resources.Add;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnAdd.Location = new System.Drawing.Point(12, 27);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 369);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.menuStrip1);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "View";
            this.Text = "Password Keeper";
            this.Load += new System.EventHandler(this.View_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemNew;
        private System.Windows.Forms.ToolStripMenuItem itemLoad;
        private System.Windows.Forms.ToolStripMenuItem itemSave;
        private System.Windows.Forms.ToolStripMenuItem entryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemAdd;
        private System.Windows.Forms.ToolStripMenuItem itemRemove;
        private System.Windows.Forms.ToolStripMenuItem itemEdit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.ToolStripMenuItem itemSettings;
        private System.Windows.Forms.ToolStripMenuItem securityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemCryptography;
        private System.Windows.Forms.ToolStripMenuItem itemCrAll;
        private System.Windows.Forms.ToolStripMenuItem itemCrCurrent;
        private System.Windows.Forms.ToolStripMenuItem itemCancel;
    }
}

