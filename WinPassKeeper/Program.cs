﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinPassKeeper
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            // Initialization
            IModel model = new Model();
            View fullView = new View();
            IView view = fullView;
            IController controller = new Controller(model, view);
            view.registerObserver(controller);
            controller.run();
            //view.updateAll();

            Application.Run(fullView);
        }
    }
}
