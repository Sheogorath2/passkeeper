﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinPassKeeper {
    public partial class InputBox : Form {

        public String output { get; set; }
        private bool ready = false;

        public InputBox(String title, String text) {
            InitializeComponent();
            Text = title;
            labelText.Text = text;
        }

        public bool checkReady() { return ready; }

        private void btnOk_Click(object sender, EventArgs e) {
            output = editInput.Text;
            ready = true;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            ready = true;
            output = "";
            Close();
        }
    }
}
