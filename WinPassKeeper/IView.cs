﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinPassKeeper
{
    public interface IView
    {
        void changeFontSize(int size);
        void remove();
        void registerObserver(IController controller);
        void updateAll();
        void selectElement(int index);
        bool checkPasswordDeletion();
        bool checkFileClear();
        String getOpenJSONFileName();
        String getSaveJSONFileName();
    }
}
