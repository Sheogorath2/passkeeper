﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WinPassKeeper
{
    public interface IController
    {

        void run();
        String[] getPasswords();
        Password getPassword(int index);
        void loadSettings();
        void saveSettings();
        void appllySettings();
        Settings getSettings();
        void setSettings(Settings settings);

        // Processes
        bool getCryptionStatus();
        void ProcessLoadFile();
        void ProcessSaveFile();
        void ProcessClearFile();
        void ProcessAddEntry(String description, String username, String password);
        void ProcessDelEntry(int index);
        void ProcessChangeEntry(int index, String description, String username, String password);
        void ProcessCryptEntry(int index, String CrPassword);
        void ProcessCryptEntries(String CrPassword);
        void ProcessCancelCryption();
    }
}
