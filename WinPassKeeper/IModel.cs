﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinPassKeeper
{
    interface IModel
    {
        Password getPassword(int index);
        void addPassword(Password password);
        void removePassword(int index);
        int Count();
    }
}
