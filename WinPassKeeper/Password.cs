﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinPassKeeper
{
    public class Password
    {
        public String description { get; set; }
        public String username { get; set; }
        public String password { get; set; }

        public Password(String description, String username, String password)  {
            this.description = description;
            this.username = username;
            this.password = password;
        }
    }
}
