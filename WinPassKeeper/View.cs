﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WinPassKeeper
{
    public partial class View : Form, IView
    {
        private IController controller;

        public View() {
            InitializeComponent();
        }

        public void registerObserver(IController controller) {
            this.controller = controller;
        }

        public void updateAll() {
            listBox1.BeginUpdate();
            listBox1.Items.Clear();
            String[] passArray = controller.getPasswords();
            for (int i = 0; i < passArray.Count(); i++ )
                this.listBox1.Items.Add(passArray[i]);
            listBox1.EndUpdate();
            if (controller.getCryptionStatus())
                itemCancel.Enabled = true;
            else
                itemCancel.Enabled = false;  
        }

        public void changeFontSize(int size) {
            this.listBox1.Font = new Font(listBox1.Font.Name, size);
        }

        public String getMasterPassword() {
            InputBox dialog = new InputBox("Master password", "Please enter master password");
            dialog.ShowDialog();
            while (true)
                if (dialog.checkReady())
                    return dialog.output;
        }

        private void View_Load(object sender, EventArgs e) {
            controller.loadSettings();
        }

        public String getOpenJSONFileName() {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "JSON schemes (*.json)|*.json|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    return openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    return "";
                }
            }
            else return "";
        }

        public String getSaveJSONFileName()   {
             SaveFileDialog saveFileDialog1 = new SaveFileDialog();

             saveFileDialog1.Filter = "JSON schemes (*.json)|*.json|All files (*.*)|*.*";
             saveFileDialog1.FilterIndex = 1;
             saveFileDialog1.RestoreDirectory = true;

             if(saveFileDialog1.ShowDialog() == DialogResult.OK)
             {
                return saveFileDialog1.FileName;
             }
             return "";
        }

        public void selectElement(int index) {
            if (index >= 0)
                listBox1.SetSelected(index, true);          
        }

        public bool checkPasswordDeletion() {
            DialogResult dr = MessageBox.Show("Delete Password?",
          "", MessageBoxButtons.YesNo);
            switch (dr)
            {
                case DialogResult.Yes:
                    return true;
                    break;
                case DialogResult.No:
                    return false;
                    break;
            }
            return false;
        }

        public bool checkFileClear() {
            DialogResult dr = MessageBox.Show("Create new file?",
          "", MessageBoxButtons.YesNo);
            switch (dr) {
                case DialogResult.Yes:
                    return true;
                    break;
                case DialogResult.No:
                    return false;
                    break;
            }
            return false;
        }

        // New menu item
        private void itemNew_Click(object sender, EventArgs e) {
            controller.ProcessClearFile();
        }

        // Save menu item
        private void itemSave_Click(object sender, EventArgs e) {
            controller.ProcessSaveFile();
        }

        // Load menu item
        private void itemLoad_Click(object sender, EventArgs e) {
            controller.ProcessLoadFile();
        }

        // Add button
        private void btnAdd_Click(object sender, EventArgs e) {
            add();
        }

        // Add menu item
        private void itemAdd_Click(object sender, EventArgs e) {
            add();
        }

        public void add() {
            AddPasswordDialog dialog = new AddPasswordDialog(true, listBox1.SelectedIndex);
            dialog.registerObserver(controller);
            dialog.ShowDialog();
        }

        // Remove button
        private void btnRemove_Click(object sender, EventArgs e) {
            remove();
        }

        // Remove menu item
        private void itemRemove_Click(object sender, EventArgs e) {
            remove();
        }

        public void remove() {
            controller.ProcessDelEntry(listBox1.SelectedIndex);
        }

        // Edit button
        private void btnEdit_Click(object sender, EventArgs e) {
            edit();
        }

        // Edit menu item
        private void itemEdit_Click(object sender, EventArgs e) {
            edit();
        }

        public void edit() {
            if (listBox1.SelectedItems.Count != 0) {
                AddPasswordDialog dialog = new AddPasswordDialog(false, listBox1.SelectedIndex,
                    controller.getPassword(listBox1.SelectedIndex).description,
                    controller.getPassword(listBox1.SelectedIndex).username,
                    controller.getPassword(listBox1.SelectedIndex).password);
                dialog.registerObserver(controller);
                dialog.ShowDialog();
            }
        }

        private void itemSettings_Click(object sender, EventArgs e) {
            SettingsDialog s = new SettingsDialog();
            s.registerObserver(controller);
            s.ShowDialog();
        }

        private void itemCryptography_Click(object sender, EventArgs e) {
            try {
                String password = getMasterPassword();
                if (password != "")
                    controller.ProcessCryptEntry(listBox1.SelectedIndex, password);
            }
            catch (Exception) {

                throw;
            }
        }

        private void itemCrAll_Click(object sender, EventArgs e) {
            try {
                String password = getMasterPassword();
                if (password != "")
                    controller.ProcessCryptEntries(password);
            }
            catch (Exception) {

                throw;
            }
        }

        private void itemCrCurrent_Click(object sender, EventArgs e) {
            try {
                String password = getMasterPassword();
                if (password != "" && listBox1.SelectedIndex >= 0)
                    controller.ProcessCryptEntry(listBox1.SelectedIndex, password);
            }
            catch (Exception) {

                throw;
            }
        }

        private void itemCancel_Click(object sender, EventArgs e) {
            controller.ProcessCancelCryption();
        }

    }
}
