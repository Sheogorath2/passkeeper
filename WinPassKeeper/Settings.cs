﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinPassKeeper {
    public class Settings {
        public int fontSize { get; set; }

        public void restoreDefault() {
            fontSize = 8;
        }

    }
}
